import QtQuick 2.12
import QtQuick.Controls 2.12 as QQC2
import Ubuntu.Components 1.3

Row {
    id: toolbarActions

    property alias model: repeater.model
    
    spacing: units.gu(2)
    
    Repeater {
        id: repeater
        
        QQC2.ToolButton {
            anchors.bottom: parent.bottom
            height: units.gu(5)
            width: height
            focusPolicy: Qt.TabFocus
            visible: modelData.visible
            contentItem: Icon {
                id: icon
                
                name: modelData ? modelData.iconName : ""
                implicitWidth: units.gu(5)
                implicitHeight: implicitWidth
                color: theme.palette.normal.foregroundText
            }

            onClicked: {
				modelData.trigger(false)
			}
        }
    }
}
